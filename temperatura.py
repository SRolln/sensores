# Para construir esta clase se ha utilizado el script de ejmplo de:
# https://electrosome.com/ds18b20-sensor-raspberry-pi-python/

import os                                                  # import os module
import glob                                                # import glob module
import time                                                # import time module
import MySQLdb
import time



class Temperatura():

    def __init__(self):
        os.system('modprobe w1-gpio')                              # Cargar los servicios 1Wire.
        os.system('modprobe w1-therm')
        self.directorio = '/sys/bus/w1/devices/'                   # directorio del fichero.
        self.carpeta = glob.glob(self.directorio + '28*')[0]       # buscar una carpeta que comience con 28*.
        self.fichero = self.carpeta + '/w1_slave'                  # fichero de escritura.


    def leer_fichero(self):
       f = open(self.fichero, 'r')
       lines = f.readlines()                                   # leer el servicio donde escribirá el sensor
       f.close()
       return lines

    def leer_temperatura_fichero(self):
       lines = self.leer_fichero()
       while lines[0].strip()[-3:] != 'YES':                   # se ignora la primera linea
          time.sleep(0.2)
          lines = self.read_temp_raw()
       equals_pos = lines[1].find('t=')                        # buscar temperatura
       if equals_pos != -1:
          temp_string = lines[1][equals_pos+2:]
          temp_c = float(temp_string) / 1000.0                 
          # El sensor solo enviará números, no caracteres, y por tanto no puede enviar decimales.
          # EJ: para mandar 25.042, el sensor mandará 25042. Por eso se divide entre 1000.
          return temp_c


    def leerTemperatura(self):
        temp = self.leer_temperatura_fichero()
        temp = round(temp, 2)
        return temp
