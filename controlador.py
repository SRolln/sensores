import temperatura as Temperatura
import caudal as Caudal
import luz as Luz
import rele as Rele

from baseDatos import BaseDatos
from conection import Post
from conection import Get
from conection import Telegram

import sys
import time

import random

mediaTemperatura = 0
mediaCaudal = 0
mediaLumenes = 0
numeroLecturasTemperatura = 0
numeroLecturasCaudal = 0
numeroLecturasLumenes = 0

# Crear objeto encargado de trabajar con el servidor remoto.
post = Post()
get = Get()
telegram = Telegram()

#Objetos para los sensores:
Temp = Temperatura.Temperatura()
Caudal = Caudal.Caudal()
Luz = Luz.Luz()
Rele = Rele.Rele()

on = False


while 1:
    time.sleep(1)

#-------- TEMPERATURA ---------------------------------------------------------
    # Lectura de la Base de Datos
    escritura = get.getProximaLectura(tipo="Temperatura") # Representa cuanto falta para laz proxima lectura.
    intervalo_lectura = get.getIntervaloLectura(tipo="Temperatura") # Representa cada cuanto son las lecturas.
    # Comprobación de error
    if escritura == -1 or intervalo_lectura == -1:
        print("exit intervalos temperatura")
        sys.exit()

    # leer Temperatura:
    temperatura_leida = Temp.leerTemperatura()
    #temperatura_leida = random.randrange(15,35)


    # acumular dato en media
    mediaTemperatura += temperatura_leida
    # incrementar numero lecturas
    numeroLecturasTemperatura += 1

    # si toca escribir en BD:
    if escritura <= 0:
        # calcular media.
        mediaTemperatura = mediaTemperatura/numeroLecturasTemperatura

        # escribir linea en BD:
        result = post.postLectura(mediaTemperatura, tipo="Temperatura")
        if result == -1:
            print("exit post temperatura")
            sys.exit()

        # comprobar aviso:
        nivel_aviso_superior = get.leerAvisoSuperior(tipo="Temperatura")
        nivel_aviso_inferior = get.leerAvisoInferior(tipo="Temperatura")
        if mediaTemperatura > nivel_aviso_superior:
            # Enviar aviso temperatura demasiado alta.
            texto = "ALERTA: La Temperatura del acuario es demasiado ALTA, temperatura actual: " +str(mediaTemperatura)
            telegram.send(texto)
            #LOG
        elif mediaTemperatura < nivel_aviso_inferior :
            # Enviar aviso temperatura demasiado baja.
            texto = "ALERTA: La Temperatura del acuario es demasiado BAJA, temperatura actual: " +str(mediaTemperatura)
            telegram.send(texto)
            # LOG
        print("Escribo temperatura: ", mediaTemperatura)
        # reiniciar variables media y numero lecturas:
        if post.rebootReads(intervalo_lectura, tipo="Temperatura") == -1:
            print("exit rebootReads temperatura")
            sys.exit()

        # reset de media
        numeroLecturasTemperatura = 0
        mediaTemperatura = 0

#-------- FIN TEMPERATURA -----------------------------------------------------


#------------ CAUDAL ----------------------------------------------------------
    # Lectura de la Base de Datos
    escritura = get.getProximaLectura(tipo='Caudal')
    intervalo_lectura = get.getIntervaloLectura(tipo='Caudal')
    # Comprobación de error
    if escritura == -1 or intervalo_lectura == -1:
        print("exit intervalos caudal")
        sys.exit()

    # leer Caudal:
    caudal_leido = Caudal.leerCaudal()
    #caudal_leido = random.randrange(70,85)

    # acumular dato en media
    mediaCaudal += caudal_leido
    # incrementar numero lecturas
    numeroLecturasCaudal += 1

    # si toca escribir en BD:
    if escritura <= 0:
        # calcular media.
        mediaCaudal = mediaCaudal/numeroLecturasCaudal

        # escribir linea en BD:
        result = post.postLectura(mediaCaudal, tipo="Caudal")
        if result == -1:
            print("exit post caudal")
            sys.exit()

        #comprobar aviso:
        nivel_aviso_inferior = get.leerAvisoInferior(tipo="Caudal")
        if mediaCaudal < nivel_aviso_superior : # Pasar al porcentaje

            # Enviar aviso temperatura demasiado baja.
            texto = "ALERTA: El Caudal del acuario es escaso, es necesario realizar una limpieza \n caudal actual: " +str(mediaCaudal)
            telegram.send(texto)
        print("Escribo caudal: ", mediaCaudal)


        # reiniciar variables media y numero lecturas:
        if post.rebootReads(intervalo_lectura, tipo="Caudal") == -1:
            print("exit rebootReads caudal")
            sys.exit()

        # reset de media
        numeroLecturasCaudal = 0
        mediaCaudal = 0

#------------ FIN CAUDAL -------------------------------------------------------



#------------ LUMENES ----------------------------------------------------------
    # Lectura de la Base de Datos
    escritura = get.getProximaLectura(tipo='Lumenes')
    intervalo_lectura = get.getIntervaloLectura(tipo='Lumenes')
    # Comprobación de error
    if escritura == -1 or intervalo_lectura == -1:
        print("exit intervalos lumenes")
        sys.exit()

    # leer Caudal:

    lumenes_leidos = Luz.leerLumenes()

    #lumenes_leidos = random.randrange(80,100)

    # acumular dato en media
    mediaLumenes += lumenes_leidos
    # incrementar numero lecturas
    numeroLecturasLumenes += 1

    # si toca escribir en BD:
    if escritura <= 0:
        # calcular media.
        mediaLumenes = mediaLumenes/numeroLecturasLumenes

        # escribir linea en BD:
        result = post.postLectura(mediaLumenes, tipo="Lumenes")
        if result == -1:
            print("exit post lumenes")
            sys.exit()

        #comprobar aviso:
        nivel_aviso_superior = get.leerAvisoSuperior(tipo="Lumenes")
        nivel_aviso_inferior = get.leerAvisoInferior(tipo="Lumenes")
        hora_inicio = get.getHoraInicio()
        hora_fin = get.getHoraFin()
        hora = time.strftime("%H:%M")


        # Si hay demasiada luz y estamos en horario:
        if mediaLumenes > nivel_aviso_superior and on == True and hora > hora_inicio and hora < hora_fin: # Si la hora está dentro del rango
            # Enviar aviso temperatura demasiado alta.
            texto = "ALERTA: Hay demasiada Luz en el acuario, las luces se apagarán. Lumenes: " +str(mediaLumenes) # Si es necesario apagar luz, avisar de ello.
            telegram.send(texto)
            #LOG

            # OFF:
            on = False
            Rele.off()


        # Si la luz es insuficiente y estamos en horario:
        elif mediaLumenes < nivel_aviso_inferior and on == False and hora > hora_inicio and hora < hora_fin: # Si la hora está dentro del rango
            # Enviar aviso temperatura demasiado baja.
            texto = "ALERTA: Hay poca Luz en el acuario, las luces se encenderán. Lumenes: " +str(mediaLumenes)
            telegram.send(texto)
            # LOG

            # ON:
            on = True
            Rele.on()

        # Si estamos fuera de horario apagamos luces.
        elif hora < hora_inicio or hora > hora_fin:
            # OFF:
            on = False
            Rele.off()

        # elif la hora esta fuera del rango: OFF
        # Comprobar porcentajes
        print("Escribo lumenes: ", mediaLumenes)


        # reiniciar variables media y numero lecturas:
        if post.rebootReads(intervalo_lectura, tipo="Lumenes") == -1:
            print("exit rebootReads lumenes")
            sys.exit()

        # reset de media
        numeroLecturasLumenes = 0
        mediaLumenes = 0

#------------ FIN LUMENES -------------------------------------------------------
