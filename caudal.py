# Esta clase está basada en el script de ejemplo de:
#    https://raspberrypi.stackexchange.com/questions/62300/flow-meter-raspberry-circuit-and-problem-receiving-pulses


import RPi.GPIO as GPIO
import time, sys

class Caudal():

    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(32, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        self.count = 0
        self.start_counter = 0
        #callback=partial(self.countPulse)

        GPIO.add_event_detect(32, GPIO.FALLING, callback=lambda x: self.countPulse())
                # Función lambda. Permite definir funciones de forma rápida.
                # Aquí se define una función que serí como esta:
                #   def x ():
                #       return self.countPulse()
                # pero lambda permite definirla entre otras cosas en la propia llamada a otra función.

    def countPulse(self):
        if self.start_counter == 1:
          self.count = self.count+1

    def leerCaudal(self):
        
        self.start_counter = 1
        time.sleep(1)
        self.start_counter = 0
        flow = (self.count * 60 * 2.25 / 1000) * 60 # Transformación correcta
        flow = round(flow, 2)
        self.count = 0
        return flow
