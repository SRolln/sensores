# Esta clase esta basada en el scrip proporcionado en:
#       


import smbus
import time


class Luz():

    def __init__(self):
        # Get I2C bus
        self.bus = smbus.SMBus(1)

    def leerLumenes(self):
        self.bus.write_byte_data(0x39, 0x00 | 0x80, 0x03)
        self.bus.write_byte_data(0x39, 0x01 | 0x80, 0x02)

        time.sleep(0.5)
        self.data = self.bus.read_i2c_block_data(0x39, 0x0C | 0x80, 2)
        self.data1 = self.bus.read_i2c_block_data(0x39, 0x0E | 0x80, 2)

        # Convert the data
        fullSpectrumn = self.data[1] * 256 + self.data[0]
        infrared = self.data1[1] * 256 + self.data1[0]
        lumenes = round(fullSpectrumn - infrared, 2)

        return lumenes
