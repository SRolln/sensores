
from conection import Post
from conection import Get

import time
import sys

# Crear objeto encargado de trabajar con el servidor remoto.
post = Post()
get = Get()

while 1:
    # Lectura de la Base de Datos
    escritura_temperatura = get.getProximaLectura(tipo="Temperatura") # Representa cuanto falta para laz proxima lectura.
    escritura_caudal = get.getProximaLectura(tipo="Caudal") # Representa cuanto falta para laz proxima lectura.
    escritura_lumenes = get.getProximaLectura(tipo="Lumenes") # Representa cuanto falta para laz proxima lectura.

    # Espera cada minuto.
    time.sleep(60)

    # decrementar contador de BD.
    if escritura_temperatura > 0:
        escritura_temperatura -= 1
    if escritura_caudal > 0:
        escritura_caudal -= 1
    if escritura_lumenes > 0:
        escritura_lumenes -= 1
    if post.setProximaLectura(escritura_temperatura, tipo = "Temperatura") == -1:
        sys.exit()
    if post.setProximaLectura(escritura_caudal, tipo = "Caudal") == -1:
        sys.exit()
    if post.setProximaLectura(escritura_lumenes, tipo = "Lumenes") == -1:
        sys.exit()
