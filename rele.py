import RPi.GPIO as gpio

class Rele():
    def __init__(self):
        self.pin = 12
        gpio.setmode(gpio.BOARD)
        gpio.setup(self.pin, gpio.OUT)

    def on(self):
        gpio.output(self.pin, True)

    def off(self):
        gpio.output(self.pin, False)
