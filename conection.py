import requests


class Post():
    def __init__(self):
        self.url = "http://192.168.1.87:8000/"
        self.contrasena = "Sar-8464"

    def postLectura(self, valor, tipo="None"):
        if tipo == "Temperatura":
            metodo = "add_Temperatura"
        elif tipo == "Caudal":
            metodo = "add_Caudal"
        elif tipo == "Lumenes":
            metodo = "add_Lumenes"
        else:
            return -1

        # Se realiza la escritura
        try:
            datos={'valor': valor, 'user_pk':1, 'contrasena': self.contrasena}
            url_completa = self.url + metodo
            r = requests.post(url = url_completa, params = datos)
        except:
             return -1


    def rebootReads(self, intervalo_lectura, tipo="None"):
        # Se realiza la escritura:
        try:
            datos={'user_pk':1, 'intervalo_lectura': intervalo_lectura, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
            url_completa = self.url + "rebootReads"
            r = requests.post(url = url_completa, params = datos)
            if r == 'ERROR':
                return -1
            return r.text
        except:
            return -1


    def setProximaLectura(self, proxima_lectura, tipo = "None"):
        # Se realiza la escritura:
        try:
            datos={'user_pk':1, 'proxima_lectura': proxima_lectura, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
            url_completa = self.url + "setProximaLectura"
            r = requests.post(url = url_completa, params = datos)
            if r == 'ERROR':
                return -1
            return r.text
        except:
            return -1


class Get():
    def __init__(self):
        self.url = "http://192.168.1.87:8000/"

    def getProximaLectura(self, tipo="None"):
        # Se realiza la lectura
        try:
            datos={'user_pk':1, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
            url_completa = self.url + "getProximaLectura"
            r = requests.get(url = url_completa, params=datos)
            if r == 'ERROR':
                return -1
            # Coger numero de la respuesta
            print('proxima: ', int(r.text))
            return int(r.text)
        except:
            return -1


    def getIntervaloLectura(self, tipo = 'None'):
        # Se realiza la lectura
        datos={'user_pk':1, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
        try:
            url_completa = self.url + "getIntervaloLectura"
            r = requests.get(url = url_completa, params=datos)
            if r.text == 'ERROR':
                return -1
            print('intervalo: ', int(r.text))
            return int(r.text)
        except:
             return -1


    def leerAvisoSuperior(self, tipo = "None"):
        # Se realiza la lectura
        datos={'user_pk':1, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
        try:
            url_completa = self.url + "getAvisoSuperior"
            r = requests.get(url = url_completa, params=datos)
            if r.text == 'ERROR':
                return -1
            return int(r.text)
        except:
            return -1

    def leerAvisoInferior(self, tipo = "None"):
        # Se realiza la lectura
        datos={'user_pk':1, 'tipo_sensor': tipo, 'contrasena': self.contrasena}
        try:
            url_completa = self.url + "getAvisoInferior"
            r = requests.get(url = url_completa, params=datos)
            if r.text == 'ERROR':
                return -1
            return int(r.text)
        except:
            return -1


    def getHoraInicio(self):
        datos={'user_pk':1, 'contrasena': self.contrasena}
        try:
            url_completa = self.url + "getHoraInicio"
            r = requests.get(url = url_completa, params=datos)
            if r.text == 'ERROR':
                return -1
            return r.text
        except:
            return -1

    def getHoraFin(self):
        datos={'user_pk':1, 'contrasena': self.contrasena}
        try:
            url_completa = self.url + "getHoraFin"
            r = requests.get(url = url_completa, params=datos)
            if r.text == 'ERROR':
                return -1
            return r.text
        except:
            return -1


class Telegram ():
    def __init__(self):
        self.token='863705445:AAGU0RZIk7J261gsW89FSZV91WrxjDyl3IA'
        self.chat_id='534016836'


    def send(self, texto):
        requests.post('https://api.telegram.org/bot'+self.token+'/sendMessage',
                      data={'chat_id': self.chat_id, 'text': texto})
